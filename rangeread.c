#include<unistd.h>
#include<stdio.h>
#include<math.h>
int range_func(int[],int);
int iqr_func(int[],int,int);
float sd_func(int[],int);
int cv_func(int[],int);
void main()
{
	int n,a[50],i,range,b[50],temp,c,j,total;
	float ir,cv;
	float sd=0;
	printf("\n Enter the limit");
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		printf("\n Enter the Number:\t ");
		scanf("%d",&a[i]);
	}
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				temp=a[i];
				a[i]=a[j];
				a[j]=temp;
			}
		}
	}
	printf("\n Enter your choice:\n 1.Range \n 2. Interquartile range \n 3.Standard Deviation \n 4.Coefficient of Variation\n");
	scanf("%d",&c);
	switch(c)
	{
		case 1 : range_func(a,n);
			 break;
		case 2 : iqr_func(a,n,total);
			 break;
		case 3 : sd=sd_func(a,n);
			 printf("\n Standard Deviation:%f\t\n ",sd);
			 break;
		case 4 : cv_func(a,n);
			 break;
		default :printf("\n Wrong Choice");
	}
}


int range_func(int a[],int n){
	int range=a[n-1]-a[0];
			 printf("\n Range:%d\t\n ",range);
	return 0;
}
	

int iqr_func(int a[], int n, int total)
{
	if(n%2==0){
		total = n;
	}
	else{
		total = n+1;
	}
	int lower = (.25)*total;
	printf("\n Total: %d\t",lower);
	int upper = (.75)*total;
	float lower_value = (a[lower]+a[lower+1])/2;
	float upper_value = (a[upper]+a[upper+1])/2;
	float total_value = upper_value-lower_value;
	printf("\n IQR: %f\t\n",total_value);
}

float sd_func(int a[], int n){
	float avg,ch,var;
	float sd,b[50];
	int i;
	for(i=0;i<n;i++)
				avg=avg+a[i];
			 avg=avg/n;
			 for(i=0;i<n;i++)
			 {
				ch=a[i]-avg;
				ch=ch*ch;
				b[i]=ch;
			 }
			 for(i=0;i<n;i++)
				var=var+b[i];
			 var=var/n;
			 sd=sqrt(var);
			return sd;
			 
}
int cv_func(int a[], int n)
{
	int i;
	float avg;
	float sd,cv;
	for(i=0;i<n;i++)
		avg=avg+a[i];
	avg=avg/n;
	sd= sd_func(a,n);
	cv=sd/avg;
	printf("\n Coefficient of relation: %f\t\n",cv);
}